package oocl.afs.todolist.controller;

import oocl.afs.todolist.entities.Todo;
import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping()
    public List<TodoResponse> findAll() {
        return todoService.findAll();
    }

    @GetMapping("/{id}")
    public TodoResponse findById(@PathVariable("id") Long id) {
        return todoService.findById(id);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Todo createTodo(@RequestBody TodoRequest request) {
        return todoService.createTodo(request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable("id") Long id) {
        todoService.deleteTodo(id);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable("id") Long id, @RequestBody TodoRequest request) {
        return todoService.updateTodo(id, request);
    }

}
