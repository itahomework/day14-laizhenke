package oocl.afs.todolist.service.mapper;

import oocl.afs.todolist.entities.Todo;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public TodoMapper() {
    }

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }
}
