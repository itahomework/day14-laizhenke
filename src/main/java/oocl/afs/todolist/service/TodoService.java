package oocl.afs.todolist.service;

import oocl.afs.todolist.entities.Todo;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.dto.TodoResponse;

import java.util.List;

public interface TodoService {

    List<TodoResponse> findAll();

    Todo createTodo(TodoRequest todoRequest);

    void deleteTodo(Long id);

    TodoResponse updateTodo(Long id, TodoRequest request);

    TodoResponse findById(Long id);
}
