package oocl.afs.todolist.service.impl;

import oocl.afs.todolist.entities.Todo;
import oocl.afs.todolist.exception.TodoNotFoundException;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoServiceImpl implements TodoService {

    private final TodoRepository todoRepository;

    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }


    @Override
    public List<TodoResponse> findAll() {
        return todoRepository.findAll().stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public Todo createTodo(TodoRequest todoRequest) {
        return todoRepository.save(todoRequest.toEntity());
    }

    @Override
    public void deleteTodo(Long id) {
        todoRepository.deleteById(id);
    }

    @Override
    public TodoResponse updateTodo(Long id, TodoRequest todoRequest) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        if (todoOptional.isEmpty()) {
            throw new TodoNotFoundException();
        }
        Todo todo = todoOptional.get();
        if (todoRequest.getName() != null) {
            todo.setName(todoRequest.getName());
        }
        if (todoRequest.getDone() != null) {
            todo.setDone(todoRequest.getDone());
        }
        Todo savedTodo = todoRepository.save(todo);
        return TodoMapper.toResponse(savedTodo);
    }

    @Override
    public TodoResponse findById(Long id) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        if (todoOptional.isEmpty()) {
            throw new TodoNotFoundException();
        }
        return TodoMapper.toResponse(todoOptional.get());
    }
}
