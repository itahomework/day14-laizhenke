package oocl.afs.todolist.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import oocl.afs.todolist.entities.Todo;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TodoRequest {
    private String name;
    private Boolean done;

    public Todo toEntity() {
        return Todo.builder().id(null).name(name).done(done).build();
    }
}
