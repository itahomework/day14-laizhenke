package oocl.afs.todolist.exception;

public class TodoNotFoundException extends RuntimeException {

    public TodoNotFoundException(String message) {
        super(message);
    }

    public TodoNotFoundException() {
        super("todo is not exist!");
    }

}
