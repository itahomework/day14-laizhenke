package oocl.afs.todolist.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import oocl.afs.todolist.entities.Todo;
import oocl.afs.todolist.exception.TodoNotFoundException;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todo_when_create_todo_given_todo() throws Exception {
        TodoRequest todo = generateTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos").
                        contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()));
    }

    @Test
    void should_return_todo_response_when_findAll() throws Exception {
        Todo todo1 = generateTodo();
        Todo todo2 = generateTodo();
        Todo savedTodo1 = todoRepository.save(todo1);
        Todo savedTodo2 = todoRepository.save(todo2);

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedTodo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(savedTodo1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(savedTodo2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(savedTodo2.getName()))
        ;
    }

    @Test
    void should_delete_todo_when_deleteTodo_given_id() throws Exception {
        Todo todo = generateTodo();
        Todo savedTodo = todoRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoRepository.findById(savedTodo.getId()).isEmpty());
    }

    @Test
    void should_return_todo_when_updateTodo_given_newTodo() throws Exception {
        Todo todo = generateTodo();
        Todo savedTodo = todoRepository.save(todo);
        TodoRequest todoRequest = new TodoRequest("sleep", true);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);
        mockMvc.perform(put("/todos/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
        Optional<Todo> todoOptional = todoRepository.findById(savedTodo.getId());
        assertTrue(todoOptional.isPresent());
        Todo updatedTodo = todoOptional.get();
        assertEquals(savedTodo.getId(), updatedTodo.getId());
        assertEquals(todoRequest.getName(), updatedTodo.getName());
        assertEquals(todoRequest.getDone(), updatedTodo.getDone());
    }

    @Test
    void should_return_todo_when_findById_given_id() throws Exception {
        Todo todo = generateTodo();
        Todo savedTodo = todoRepository.save(todo);

        mockMvc.perform(get("/todos/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(savedTodo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(savedTodo.getDone()));
    }

    private static TodoRequest generateTodoRequest() {
        return TodoRequest.builder().name("study").done(false).build();
    }

    private static Todo generateTodo() {
        return Todo.builder().name("study").done(false).build();
    }

}
