## Daily Report(07/27)

- O: code review, built our own backend, when we connected the front-end and back-end, we encountered cross domain issues, ppt sharing.
- R: learn more things.
- I: By reviewing the code of others, I learned how to implement responsive layout. The students in the second group implemented responsive layout through the @ media tag of CSS. The code shown in our group was implemented through Antd's Layout component. In the afternoon's PPT sharing, I didn't have a deep understanding of the concept of agile development, looking forward to Teacher Yuan's explanation on agile development tomorrow.
- D: learn more about Agile development.